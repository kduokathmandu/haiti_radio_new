README
======

Radio App for Android
----------------------------------

Which Permissions does   need?
---------------------------------------

### Permission "INTERNET"
  streams radio stations over the internet.


### Permission "VIBRATE"
Tapping and holding a radio station will toggle a tiny vibration.

### Permission "WAKE_LOCK"
During Playback   acquires a so called partial wake lock. That prevents the Android system to stop playback for power saving reasons.
